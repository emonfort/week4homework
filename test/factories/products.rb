# frozen_string_literal: true

FactoryBot.define do
  factory :product do
    id { Faker::Number.number(digits: 2) }
    name { Faker::Commerce.product_name }
    sku { Faker::Number.number(digits: 5) }
    price { Faker::Commerce.price(range: 10..100.0) }
    stock { Faker::Number.between(from: 1, to: 25) }
  end
end
