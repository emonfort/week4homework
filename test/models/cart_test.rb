# frozen_string_literal: true

require 'test_helper'

# Tests for cart model
class CartTest < ActiveSupport::TestCase
  def setup
    @cart = Cart.new(id: 1)
  end

  def test_add_product_first_time_to_cart_sets_quantity_to_one
    product = FactoryBot.create(:product)
    Product::AddToCart.call(@cart, product)
    assert @cart.order_items.first.quantity == 1
  end
end
