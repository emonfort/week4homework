# frozen_string_literal: true

require 'test_helper'

# Tests for order items model
class OrderItemTest < ActiveSupport::TestCase
  def setup
    @order_item = OrderItem.new(id: 1)
    @product = FactoryBot.create(:product)
  end

  def test_total_price_when_quantity_one_equals_product_price
    @order_item.product = @product
    @order_item.quantity = 1
    assert @order_item.total_price == @product.price
  end

  def test_total_price_when_quantity_two_equals_product_price_by_two
    @order_item.product = @product
    @order_item.quantity = 2
    assert @order_item.total_price == 2 * @product.price
  end

  def test_reduce_stock_reduces_stock_of_product
    @order_item.product = @product
    @order_item.quantity = 1
    new_stock = @product.stock - @order_item.quantity
    Product::ReduceStock.call(@order_item.product, @order_item.quantity)
    assert @product.stock == new_stock
  end
end
