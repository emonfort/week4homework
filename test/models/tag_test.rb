# frozen_string_literal: true

require 'test_helper'

# Tests for order items model
class TagTest < ActiveSupport::TestCase
  def setup
    @tag = FactoryBot.create(:tag)
  end

  def test_product_with_name_is_valid
    assert @tag.valid?
  end

  def test_product_without_name_is_invalid
    @tag.name = nil
    assert_not @tag.valid?
  end
end
