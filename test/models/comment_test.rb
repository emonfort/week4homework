# frozen_string_literal: true

require 'test_helper'

# Tests for comments model
class CommentTest < ActiveSupport::TestCase
  def setup
    @comment = Comment.new(id: 1, commentable_type: 'Product', commentable_id: 10)
  end

  def test_comment_without_description_is_invalid
    assert_not @comment.valid?
  end
end
