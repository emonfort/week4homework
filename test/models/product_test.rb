# frozen_string_literal: true

require 'test_helper'

# Tests for product model
class ProductTest < ActiveSupport::TestCase
  def setup
    @product = FactoryBot.create(:product)
  end

  def test_product_with_name_sku_price_stock_is_valid
    assert @product.valid?
  end

  def test_product_without_name_is_invalid
    @product.name = nil
    assert_not @product.valid?
  end

  def test_product_without_sku_is_invalid
    @product.sku = nil
    assert_not @product.valid?
  end

  def test_product_without_price_is_invalid
    @product.price = nil
    assert_not @product.valid?
  end

  def test_product_without_stock_is_invalid
    @product.stock = nil
    assert_not @product.valid?
  end

  def test_stock_must_be_greater_or_equal_than_zero
    @product.stock = -2
    assert_not @product.valid?
    @product.stock = 0
    assert @product.valid?
  end

  def test_price_must_be_greater_or_equal_than_point_one
    @product.price = -2
    assert_not @product.valid?
    @product.price = 0
    assert_not @product.valid?
    @product.price = 0.1
    assert @product.valid?
  end
end
