# frozen_string_literal: true

require 'test_helper'

# Tests for home controller
class HomeControllerTest < ActionDispatch::IntegrationTest
  def test_should_get_index
    get home_index_url
    assert_response :success
  end
end
