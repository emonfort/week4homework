# frozen_string_literal: true

require 'test_helper'
require 'pp'

# Tests for tag controller
class TagsControllerTest < ActionDispatch::IntegrationTest
  def test_index_action_should_be_success
    get tags_url
    assert_response :success
  end

  def test_new_action_should_be_success
    get new_tag_url
    assert_response :success
  end

  def test_show_action_should_be_success
    tag = FactoryBot.create(:tag)
    get tags_url(tag)
    assert_response :success
  end

  def test_create_tag_with_name_should_redirect
    post tags_url, params: { tag_create_form: { name: 'chocolate' } }
    assert_response 302
    assert_redirected_to "#{tags_path}/#{Tag.last.id}"
  end

  def test_create_tag_without_name_should_be_unprocessable_entity
    post tags_url, params: { tag_create_form: { name: nil } }
    assert_response 422
  end

  def test_edit_action_should_be_success
    tag = FactoryBot.create(:tag)
    get edit_tag_url(tag.id)
    assert_response 200
  end

  def test_update_changes_tag_name_succesfuly
    tag = FactoryBot.create(:tag)
    patch tag_url(tag), params: { tag_edit_form: { name: 'new name' } }
    tag.reload
    assert_equal 'new name', tag.name
    assert_response 302
    assert_redirected_to "#{tags_path}/#{Tag.last.id}"
  end

  def test_delete_action_should_redirect_to_tags_index
    tag = FactoryBot.create(:tag)
    delete tag_url(tag.id)
    assert_response 302
    assert_redirected_to tags_url
  end
end
