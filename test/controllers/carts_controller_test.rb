# frozen_string_literal: true

require 'test_helper'

# Tests for carts controller
class CartsControllerTest < ActionDispatch::IntegrationTest
  def test_show_action_should_be_success
    cart = Cart.create
    get cart_url(cart)
    assert_response :success
  end

  def test_delete_action_should_redirect_to_home_index
    cart = Cart.create
    delete cart_url(cart.id)
    assert_response 302
    assert_redirected_to home_index_url
  end
end
