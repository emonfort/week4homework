# frozen_string_literal: true

require 'test_helper'
require 'pp'

# Tests for orders controller
class OrdersControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  def test_new_action_should_be_success
    user = User.new
    sign_in user
    get new_order_url
    assert_response :success
  end

  def test_if_user_logged_in_index_action_should_be_success
    user = User.new
    sign_in user
    get orders_url
    assert_response :success
  end

  def test_if_user_not_logged_in_index_action_should_redirect
    get orders_url
    assert_response 302
    assert_redirected_to new_user_registration_url
  end
end
