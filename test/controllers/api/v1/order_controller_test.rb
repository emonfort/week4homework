# frozen_string_literal: true

require 'test_helper'

module Api
  module V1
    class OrderControllerTest < ActionDispatch::IntegrationTest
      def setup
        @user = FactoryBot.create(:user)
      end

      def test_index_action_if_correct_token_should_be_success
        token = JsonWebToken.encode(user_id: @user.id)
        get api_v1_orders_url, headers: { 'Authorization': token }
        assert_response :success
      end

      def test_index_action_if_incorrect_token_should_be_unprocessable_entity
        JsonWebToken.encode(user_id: @user.id)
        get api_v1_orders_url, headers: { 'Authorization': 'bad token' }
        assert_response :unprocessable_entity
      end
    end
  end
end
