# frozen_string_literal: true

require 'test_helper'

module Api
  module V1
    class ProductsControllerTest < ActionDispatch::IntegrationTest
      def test_index_action_should_be_success
        get api_v1_products_url
        assert_response :success
      end

      def test_show_action_should_be_successs
        product = FactoryBot.create(:product)
        get api_v1_product_url(product)
        assert_response :success
      end
    end
  end
end
