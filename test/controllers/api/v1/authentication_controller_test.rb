# frozen_string_literal: true

require 'test_helper'

module Api
  module V1
    class AuthenticationControllerTest < ActionDispatch::IntegrationTest
      def setup
        @user = FactoryBot.create(:user)
      end

      def test_login_if_correct_password_should_be_success
        post api_v1_auth_login_url params: { email: @user.email, password: @user.password }
        assert_response :success
      end

      def test_login_if_incorrect_password_should_be_unauthorized
        post api_v1_auth_login_url params: { email: @user.email, password: 'bad password' }
        assert_response :unauthorized
      end
    end
  end
end
