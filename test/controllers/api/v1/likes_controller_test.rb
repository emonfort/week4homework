# frozen_string_literal: true

require 'test_helper'

module Api
  module V1
    class LikesControllerTest < ActionDispatch::IntegrationTest
      def setup
        @user = FactoryBot.create(:user)
        @product = FactoryBot.create(:product)
      end

      def test_index_action_if_correct_token_should_be_success
        token = JsonWebToken.encode(user_id: @user.id)
        post api_v1_likes_url, params: { product_id: @product.id }, headers: { 'Authorization': token }
        assert_response :success
      end
    end
  end
end
