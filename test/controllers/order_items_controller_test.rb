# frozen_string_literal: true

require 'test_helper'
require 'pp'

# Tests for order items controller
class OrderItemsControllerTest < ActionDispatch::IntegrationTest
  def test_create_should_redirect_to_cart
    product = FactoryBot.create(:product)
    product.save
    post "#{order_items_url}?product_id=#{product.id}"
    assert_response 302
    assert_redirected_to cart_url(Cart.find(session[:cart_id]))
  end
end
