# frozen_string_literal: true

require 'test_helper'

# Tests for products controller
class ProductsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  def setup
    user = User.new(role: 'admin')
    sign_in user
  end

  def test_if_admin_logged_index_action_should_be_success
    get products_url
    assert_response :success
  end

  def test_if_user_logged_index_action_should_redirect
    user = User.new(role: 'user')
    sign_in user
    get products_url
    assert_response 302
    assert_redirected_to home_index_url
  end

  def test_new_action_should_be_success
    get new_product_url
    assert_response :success
  end

  def test_show_action_should_be_success
    product = FactoryBot.create(:product)
    get product_url(product)
    assert_response :success
  end

  def test_create_valid_product_should_redirect
    post products_url, params: { product_create_form: { name: 'chocolate', sku: 20_000, price: 10, stock: 5 } }
    assert_response 302
  end

  def test_create_product_without_name_should_be_unprocessable_entity
    post products_url, params: { product_create_form: { name: nil } }
    assert_response 422
  end

  def test_edit_action_should_be_success
    product = FactoryBot.create(:product)
    get edit_product_url(product.id)
    assert_response 200
  end

  def test_update_changes_product_name_succesfuly
    product = FactoryBot.create(:product)
    patch product_url(product), params: { product_edit_form: { name: 'new name', sku: 20_000, price: 10, stock: 15 } }
    product.reload
    assert_equal 'new name', product.name
    assert_response 302
    assert_redirected_to "#{products_path}/#{Product.last.id}"
  end

  def test_delete_action_should_redirect_to_products_index
    product = FactoryBot.create(:product)
    delete product_url(product.id)
    assert_response 302
    assert_redirected_to products_url
  end
end
