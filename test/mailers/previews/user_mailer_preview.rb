# frozen_string_literal: true

# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview
  def like_email
    @user = User.first
    @product = Product.last
    UserMailer.like_email(@user, @product)
  end
end
