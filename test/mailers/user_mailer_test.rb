# frozen_string_literal: true

require 'test_helper'

# Tests for mailer
class UserMailerTest < ActionMailer::TestCase
  def test_cosito
    user = FactoryBot.create(:user)
    product = FactoryBot.create(:product)

    email = UserMailer.like_email(user, product)

    assert_emails 1 do
      email.deliver_now
    end

    assert_equal [ENV['GMAIL_USERNAME']], email.from
    assert_equal [user.email], email.to
    assert_equal 'Product getting out of stock', email.subject
  end
end
