# README

## Setup:

install all gems:

```shell
$ bundle install
```

install Webpacker:

```shell
$ bundle exec rake webpacker:install
```

create database:

```shell
$ rails db:create
```

run migrations:

```shell
$ rails db:migrate
```

run seed (optional)

```shell
$ rails db:seed
```

run server

```shell
$ rails s
```

The application will be running in:

```shell
http://localhost:3000/
```

## Heroku deploy:

Admin email: admin@admin.com
Admin password: password

Support email: suport@mail.com
Support password: password

User email: user@mail.com
User password: password

## Documentation:

Run index.html from the docs folder to see information about the app