# frozen_string_literal: true

# Aplication mailer class
class ApplicationMailer < ActionMailer::Base
  default from: ENV['GMAIL_USERNAME']
  layout 'mailer'
end
