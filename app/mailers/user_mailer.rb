# frozen_string_literal: true

# User Mailer class
class UserMailer < ApplicationMailer
  def like_email(user, product)
    @user = user
    @product = product
    mail(to: user.email, subject: 'Product getting out of stock')
  end
end
