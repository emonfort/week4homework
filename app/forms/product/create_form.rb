# frozen_string_literal: true

# Form for create products
class Product::CreateForm
  include ActiveModel::Model

  attr_accessor :name, :sku, :stock, :price, :description, :image

  validates :name, :sku, presence: true
  validates :price, numericality: { greater_than_or_equal_to: 0.01 }
  validates :stock, numericality: { greater_than_or_equal_to: 0 }
  validate :sku_uniqueness

  def validate!
    raise CustomError.new('Invalid', 'Invalid product', :unprocessable_entity), self unless valid?

    Product.new(attributes)
  end

  def submit
    return false unless valid?

    Product.new(attributes)
  end

  def attributes
    {
      name: name,
      sku: sku,
      stock: stock,
      price: price,
      description: description,
      image: image
    }
  end

  private

  def sku_uniqueness
    errors.add(:sku, 'SKU already exists') unless Product.find_by(sku: @sku).nil?
  end
end
