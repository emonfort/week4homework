# frozen_string_literal: true

# Form object for creating tags
class Tag::CreateForm
  include ActiveModel::Model

  attr_accessor :name

  validates :name, presence: true
  validate :tag_uniqueness

  def validate!
    raise CustomError.new('Invalid', 'Invalid tag', :unprocessable_entity), self unless valid?

    Tag.new(attributes)
  end

  def submit
    return false unless valid?

    Tag.new(attributes)
  end

  def attributes
    {
      name: @name
    }
  end

  private

  def tag_uniqueness
    errors.add(:name, 'Tag already exists') unless Tag.find_by(name: @name).nil?
  end
end
