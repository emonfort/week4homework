# frozen_string_literal: true

# Form ibject for editing tags
class Tag::EditForm
  include ActiveModel::Model

  attr_accessor :name, :id

  validates :name, presence: true
  validate :tag_uniqueness

  def validate!
    raise CustomError.new('Invalid', 'Invalid edit tag', :unprocessable_entity), self unless valid?

    tag = Tag.find(id)
    tag.assign_attributes(attributes)
    tag
  end

  def submit
    return false unless valid?

    tag = Tag.find(id)
    tag.assign_attributes(attributes)
    tag
  end

  def attributes
    {
      name: name,
      id: id
    }
  end

  private

  def tag_uniqueness
    errors.add(:name, 'Tag already exists') unless Tag.find_by(name: @name).nil?
  end
end
