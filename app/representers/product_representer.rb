# frozen_string_literal: true

# User representer class
class ProductRepresenter < Representable::Decorator
  include Representable::JSON

  property :name
  property :description
  property :price
  property :stock
  collection :tags, class: Tag do
    property :name
  end
  collection :comments, class: Comment do
    property :user_id
    property :description
  end
  collection :likes, class: Like do
    property :user_id
  end
end
