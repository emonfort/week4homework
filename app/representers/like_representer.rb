# frozen_string_literal: true

# Like representer class
class LikeRepresenter < Representable::Decorator
  include Representable::JSON

  property :id
  property :user_id
  property :product_id
  property :created_at
end
