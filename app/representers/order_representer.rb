# frozen_string_literal: true

# Order representer class
class OrderRepresenter < Representable::Decorator
  include Representable::JSON

  property :id
  property :user_id
  property :total
  property :created_at
  property :status
  collection :order_items, class: OrderItem do
    property :product
    property :quantity
  end
end
