# frozen_string_literal: true

# Parent class for services
class ApplicationService
  def self.call(*args)
    new(*args).call
  end
end
