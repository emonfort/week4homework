# frozen_string_literal: true

# Service to create a product price change log
class Tag::Update < ApplicationService
  attr_reader :tag

  def initialize(tag)
    @tag = tag
  end

  def call
    if @tag
      @tag.save
      @tag
    else
      false
    end
  end
end
