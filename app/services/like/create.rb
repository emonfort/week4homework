# frozen_string_literal: true

# Service to like products
class Like::Create < ApplicationService
  attr_reader :user, :params

  # Initializer
  # Receives an +user+ and +parameters+
  def initialize(user, params)
    @user = user
    @params = params
  end

  # Create like
  # If already liked raises an error
  def call
    @like = @user.likes.new(params)

    raise CustomError.new('Like error', 'Already liked that product', :unprocessable_entity) unless @like.save

    @like
  end
end
