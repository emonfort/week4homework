# frozen_string_literal: true

# Service to reduce a stock of a product
class Product::ReduceStock < ApplicationService
  attr_reader :product, :quantity

  # Initializer
  # Receives a +product+ and an +integer+ (amount to decrease product stock)
  def initialize(product, quantity)
    @product = product
    @quantity = quantity
  end

  # Reduce the stock of a +product+ by +quantity+
  def call
    @product.stock -= @quantity
    @product.save

    @last_like = Like.where("product_id = #{@product.id}").last

    return if @last_like.nil?

    @user = User.find(@last_like.user_id)
    MailUserJob.perform_later(@user, @product)
  end
end
