# frozen_string_literal: true

# Service to create a product
class Product::Create < ApplicationService
  attr_reader :product

  def initialize(product)
    @product = product
  end

  def call
    if @product
      @product.save
      @product
    else
      false
    end
  end
end
