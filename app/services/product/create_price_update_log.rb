# frozen_string_literal: true

# Service to create a product price change log
class Product::CreatePriceUpdateLog < ApplicationService
  attr_reader :product, :old_price

  # Initializer
  # Receives a +product+ and a +double+ (old price of product)
  def initialize(product, old_price)
    @product = product
    @old_price = old_price
  end

  # Add a log to the logger if the price of a product is edited
  def call
    return if @product.price == @old_price

    price_update_log.info("#{@product.name}: old price: #{@old_price} - new price: #{@product.price}")
  end

  private

  # Generates and returns a logger for product price updates
  # If a logger exits already it returns it
  def price_update_log
    @price_update_log ||= Logger.new(Rails.root.join('log/price_update.log'))
  end
end
