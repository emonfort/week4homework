# frozen_string_literal: true

# Service to fill an order
class Product::Find < ApplicationService
  attr_reader :id

  # Initializer
  # Receives an +id+
  def initialize(id)
    @id = id
  end

  # Returns a product if exists
  # Raises a RecordNotFound if not
  def call
    @product = Product.find_by(id: @id)
    return @product if @product

    raise ActiveRecord::RecordNotFound
  end
end
