# frozen_string_literal: true

# Service to add a +product+ to a +cart+
class Product::AddToCart < ApplicationService
  attr_reader :product, :cart

  # Initializer
  # Receives a +cart+ and a +product+
  def initialize(cart, product)
    @product = product
    @cart = cart
  end

  # Add a +product+ to a +cart+
  # If there is a +product+ with the same id, it increments its quantity
  def call
    current_item = @cart.order_items.find_by(product_id: @product.id)
    if current_item
      return nil if current_item.quantity == current_item.product.stock

      current_item.quantity += 1
    else
      current_item = @cart.order_items.build(product_id: @product.id)
    end
    current_item
  end
end
