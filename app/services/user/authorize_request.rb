# frozen_string_literal: true

# Service to create order logs
class User::AuthorizeRequest < ApplicationService
  attr_reader :request

  # Initializer
  # Receives a +request+
  def initialize(request)
    @request = request
  end

  # Decode JWT
  # if correct token it returns the current user
  # if not it raises an error
  def call
    header = @request.headers['Authorization']
    header = header.split(' ').last if header
    begin
      @decoded = JsonWebToken.decode(header)
      @current_user = User.find(@decoded[:user_id])
    rescue ActiveRecord::RecordNotFound
      raise ActiveRecord::RecordNotFound
    rescue JWT::DecodeError => e
      raise CustomError.new('Token decode error', e.message, :unprocessable_entity)
    end
  end
end
