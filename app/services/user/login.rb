# frozen_string_literal: true

# Service to create order logs
class User::Login < ApplicationService
  attr_reader :email, :password

  # Initializer
  # Receives an +email+ and +password+
  def initialize(email, password)
    @email = email
    @password = password
  end

  # Log user
  # If email or password incorrect raises an error
  def call
    @user = User.find_by(email: @email)
    unless @user&.valid_password?(@password)
      raise CustomError.new('Login error', 'Incorrect email or password', :unauthorized)
    end

    JsonWebToken.encode(user_id: @user.id)
  end
end
