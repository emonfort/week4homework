# frozen_string_literal: true

# Service to destroy carts
class Cart::Destroy < ApplicationService
  attr_reader :cart, :session

  # Initializer
  # Receives an +cart+
  def initialize(cart, session)
    @cart = cart
    @session = session
  end

  # Destroys cart
  def call
    @cart.destroy if @cart.id == session[:cart_id]
    session[:cart_id] = nil
  end
end
