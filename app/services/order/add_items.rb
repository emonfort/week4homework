# frozen_string_literal: true

# Service to add items to orders
class Order::AddItems < ApplicationService
  attr_reader :cart, :order

  # Initializer
  # Receives a +cart+ an an +order+
  def initialize(cart, order)
    @order = order
    @cart = cart
  end

  # Add the +order_items+ to the +order+
  def call
    @order.total = @cart.total_price
    @cart.order_items.each do |item|
      item.cart_id = nil
      @order.order_items << item
    end
  end
end
