# frozen_string_literal: true

# Service to add items to orders
class Order::Create < ApplicationService
  attr_reader :cart, :user

  # Initializer
  # Receives a +cart+ and an +user+
  def initialize(cart, user)
    @cart = cart
    @user = user
  end

  # Creates and returns the +order+
  def call
    @order = Order.new
    Order::AddItems.call(cart, @order)
    Order::Fill.call(@order, user)
    @order.order_items.each { |order_item| Product::ReduceStock.call(order_item.product, order_item.quantity) }
    @order
  end
end
