# frozen_string_literal: true

# Service to create order logs
class Order::CreateLog < ApplicationService
  attr_reader :order

  # Initializer
  # Receives an +order+
  def initialize(order)
    @order = order
  end

  # Add a log to the logger
  def call
    log = "Customer: #{@order.user.id}\n"
    @order.order_items.each do |item|
      log += "#{item.quantity} x #{item.product.name}: #{item.total_price}\n"
    end
    log += "Total price: #{@order.total}"
    purchase_log.info(log)
  end

  private

  # Generates and returns a logger for purchases
  # If a logger exits already it returns it
  def purchase_log
    @purchase_log ||= Logger.new(Rails.root.join('log/purchase_log.log'))
  end
end
