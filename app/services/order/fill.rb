# frozen_string_literal: true

# Service to fill an order
class Order::Fill < ApplicationService
  attr_reader :order, :user

  # Initializer
  # Receives an +order+ and an +user+
  def initialize(order, user)
    @order = order
    @user = user
  end

  # Populates the order with the +user+ id, +Time+, and +status+ as 'processing'
  def call
    @order.user_id = @user.id
    @order.date = Time.zone.now
    @order.status = 'processing'
  end
end
