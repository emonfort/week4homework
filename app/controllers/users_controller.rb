# frozen_string_literal: true

# Home controller class
class UsersController < ApplicationController
  include SecureRolePermissions

  before_action :require_support, only: [:edit]
  before_action :set_user, only: %i[edit update show]

  def index
    @users = User.all
  end

  def edit; end

  def show; end

  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to users_url, notice: 'User was successfully updated.' }
      else
        format.html { render :edit, status: :unprocessable_entity }
      end
    end
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:role)
  end
end
