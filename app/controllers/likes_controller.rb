# frozen_string_literal: true

# Likes controller class
class LikesController < ApplicationController
  def create
    @like = current_user.likes.new(like_params)
    flash[:notice] = @like.errors.full_messages.to_sentence unless @like.save

    redirect_to @like.product
  end

  def destroy
    @like = current_user.likes.find(params[:id])
    product = @like.product
    @like.destroy
    redirect_to product
  end

  private

  def like_params
    params.require(:like).permit(:product_id)
  end
end
