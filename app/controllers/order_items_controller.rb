# frozen_string_literal: true

# Order items controller
class OrderItemsController < ApplicationController
  include CurrentCart

  before_action :set_cart

  def create
    product = Product.find(params[:product_id])
    @order_item = Product::AddToCart.call(@cart, product)

    respond_to do |format|
      if @order_item.nil?
        format.html { redirect_to home_index_url, notice: 'Don\'t have enough stock' }
      elsif @order_item.save
        format.html { redirect_to @order_item.cart, notice: 'Order item was successfully created.' }
      else
        format.html { render :new, status: :unprocessable_entity }
      end
    end
  end

  private

  def set_order_item
    @order_item = OrderItem.find(params[:id])
  end

  def order_item_params
    params.require(:order_item).permit(:product_id, :order_id, :cart_id)
  end
end
