# frozen_string_literal: true

# Parent class of controllers
class ApplicationController < ActionController::Base
  include Pagy::Backend

  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  # Allows to create and edit role, first_name, last_name, address, and phone of users
  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: %i[role first_name last_name address phone])
  end
end
