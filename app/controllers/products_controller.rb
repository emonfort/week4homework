# frozen_string_literal: true

# Products controller class
class ProductsController < ApplicationController
  include SecureRolePermissions

  before_action :set_product, only: %i[show edit update destroy]
  before_action :require_support, except: [:show]

  # GET /products or /products.json
  def index
    @pagy, @products = pagy(Product.all)
  end

  # GET /products/1 or /products/1.json
  def show; end

  # GET /products/new
  def new
    @product_form = Product::CreateForm.new
  end

  # GET /products/1/edit
  def edit
    @product_edit_form = Product::EditForm.new(id: params[:id])
  end

  # POST /products or /products.json
  def create
    @product_form = Product::CreateForm.new(product_params)
    @product = Product::Create.call(@product_form.submit)

    if @product
      redirect_to @product, notice: 'Product was successfully created.'
    else
      render :new, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /products/1 or /products/1.json
  def update
    @product_edit_form = Product::EditForm.new(product_edit_params.merge(id: params[:id]))
    @product = Product::Update.call(@product_edit_form.submit)
    if @product
      redirect_to @product, notice: 'Product was successfully updated.'
    else
      render :edit, status: :unprocessable_entity
    end
  end

  # DELETE /products/1 or /products/1.json
  def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to products_url, notice: 'Product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_product
    @product = Product.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def product_params
    params.require(:product_create_form).permit(:sku, :name, :description, :image, :price, :stock, tag_ids: [])
  end

  def product_edit_params
    params.require(:product_edit_form).permit(:sku, :name, :description, :image, :price, :stock, tag_ids: [])
  end
end
