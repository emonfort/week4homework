# frozen_string_literal: true

# Products controller class
class TagsController < ApplicationController
  before_action :set_tag, only: %i[show destroy]

  def index
    @tags = Tag.all
  end

  def show; end

  def new
    @tag_form = Tag::CreateForm.new
  end

  def edit
    @tag_edit_form = Tag::EditForm.new('id' => params[:id])
  end

  def create
    @tag_form = Tag::CreateForm.new(tag_params)
    @tag = Tag::Create.call(@tag_form.submit)
    if @tag
      redirect_to @tag, notice: 'Tag was successfully created.'
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    @tag_edit_form = Tag::EditForm.new(tag_edit_params.merge(id: params[:id]))
    @tag = Tag::Update.call(@tag_edit_form.submit)
    if @tag
      redirect_to @tag, notice: 'Tag was successfully created.'
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @tag.destroy
    respond_to do |format|
      format.html { redirect_to tags_url, notice: 'Tag was successfully destroyed.' }
    end
  end

  private

  def set_tag
    @tag = Tag.find(params[:id])
  end

  def tag_params
    params.require(:tag_create_form).permit(:name)
  end

  def tag_edit_params
    params.require(:tag_edit_form).permit(:name)
  end
end
