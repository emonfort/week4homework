# frozen_string_literal: true

# module that add methods to check if +user+ is +support+ or +admin+
module SecureRolePermissions
  def require_admin
    return if user_signed_in? && current_user.admin?

    redirect_to home_index_url, alert: 'Only admins can access this section'
  end

  def require_support
    return if user_signed_in? && (current_user.admin? || current_user.support?)

    redirect_to home_index_url, alert: 'Only admins and supports can acces this section'
  end
end
