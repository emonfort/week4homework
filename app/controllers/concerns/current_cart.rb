# frozen_string_literal: true

# module to look for or create a new cart when need
module CurrentCart
  private

  # Retrieves actual cart.
  # If there isn't one, creates one
  def set_cart
    @cart = Cart.find(session[:cart_id])
  rescue ActiveRecord::RecordNotFound
    @cart = Cart.create
    session[:cart_id] = @cart.id
  end
end
