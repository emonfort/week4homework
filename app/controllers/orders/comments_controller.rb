# frozen_string_literal: true

module Orders
  # Controller for comments of orders
  class CommentsController < CommentsController
    before_action :set_commentable

    private

    # Set the commentable object to the order
    def set_commentable
      @commentable = Order.find(params[:order_id])
    end
  end
end
