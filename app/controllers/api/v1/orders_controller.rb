# frozen_string_literal: true

module Api
  module V1
    # Users controller class
    class OrdersController < ApplicationController
      before_action :authorize_request

      # GET /api/v1/orders
      def index
        @orders = OrdersQuery.new.user_orders(current_user)
        render json: OrderRepresenter.for_collection.new(@orders).to_json, status: :ok
      end
    end
  end
end
