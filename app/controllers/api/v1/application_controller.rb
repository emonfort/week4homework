# frozen_string_literal: true

module Api
  module V1
    # Parent class for api controllers
    class ApplicationController < ActionController::API
      include ErrorHandler

      def authorize_request
        @current_user = User::AuthorizeRequest.call(request)
      end
    end
  end
end
