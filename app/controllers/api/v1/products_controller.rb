# frozen_string_literal: true

module Api
  module V1
    # Products controller class
    class ProductsController < ApplicationController
      # GET api/products
      def index
        @products = ProductsQuery.new.order(params[:sort])
        render json: ProductRepresenter.for_collection.new(@products).to_json, status: :ok
      end

      # GET api/products/1
      def show
        @product = Product::Find.call(params[:id])
        render json: ProductRepresenter.new(@product).to_json, status: :ok
      end
    end
  end
end
