# frozen_string_literal: true

module Api
  module V1
    # API Like controller class
    class LikesController < ApplicationController
      before_action :authorize_request

      def create
        @like = Like::Create.call(@current_user, like_params)
        render json: LikeRepresenter.new(@like).to_json
      end

      private

      def like_params
        params.permit(:product_id)
      end
    end
  end
end
