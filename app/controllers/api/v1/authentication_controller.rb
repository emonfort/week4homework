# frozen_string_literal: true

module Api
  module V1
    # Class for user authentication
    class AuthenticationController < ApplicationController
      before_action :authorize_request, except: :login

      # POST /auth/login
      def login
        token = User::Login.call(params[:email], params[:password])
        render json: { token: token }, status: :ok
      end

      private

      def login_params
        params.permit(:email, :password)
      end
    end
  end
end
