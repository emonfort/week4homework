# frozen_string_literal: true

# Cart controller class
class CartsController < ApplicationController
  before_action :set_cart, only: %i[show destroy]

  # GET /carts/1 or /carts/1.json
  def show; end

  # DELETE /carts/1 or /carts/1.json
  def destroy
    Cart::Destroy.call(@cart, session)
    redirect_to home_index_url, notice: 'Cart was successfully destroyed.'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_cart
    @cart = Cart.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def cart_params
    params.fetch(:cart, {})
  end
end
