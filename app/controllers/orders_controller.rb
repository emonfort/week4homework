# frozen_string_literal: true

# Order controller class
class OrdersController < ApplicationController
  include CurrentCart
  before_action :set_cart, only: %i[new create]
  before_action :set_order, only: %i[show destroy]
  before_action :require_logged
  before_action :check_current_user, only: [:show]

  # GET /orders or /orders.json
  def index
    @pagy, @orders = pagy(OrdersQuery.new.user_orders(current_user))
  end

  # GET /orders/1 or /orders/1.json
  def show; end

  # GET /orders/new
  def new
    @order = Order.new
  end

  # POST /orders or /orders.json
  def create
    @order = Order::Create.call(@cart, current_user)
    respond_to do |format|
      if @order.save
        Order::CreateLog.call(@order)
        Cart::Destroy.call(@cart, session)
        format.html { redirect_to home_index_url, notice: 'Order was successfully created.' }
      else
        format.html { render :new, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/1 or /orders/1.json
  def destroy
    @order.destroy
    respond_to do |format|
      format.html { redirect_to orders_url, notice: 'Order was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def set_order
    @order = Order.find(params[:id])
  end

  def require_logged
    return if user_signed_in?

    redirect_to new_user_registration_url, alert: 'Only logged users can buy products'
  end

  def check_current_user
    return if current_user.id == @order.user_id

    redirect_to home_index_url, alert: 'You don\'t have permisson to see that order'
  end
end
