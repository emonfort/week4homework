# frozen_string_literal: true

# Home controller class
class HomeController < ApplicationController
  include CurrentCart

  before_action :set_cart

  def index
    @pagy, @products = pagy(ProductsQuery.new.order(params[:sort]))
  end
end
