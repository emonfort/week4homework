# frozen_string_literal: true

module Products
  # Controller for comments of products
  class CommentsController < CommentsController
    before_action :set_commentable

    private

    # Set the commentable object to the order
    def set_commentable
      @commentable = Product.find(params[:product_id])
    end
  end
end
