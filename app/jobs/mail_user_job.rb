# frozen_string_literal: true

# Job class for sending mails to user
class MailUserJob < ApplicationJob
  queue_as :default

  def perform(user, product)
    return if product.stock > 3

    UserMailer.like_email(user, product).deliver_later
  end
end
