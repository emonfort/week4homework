# frozen_string_literal: true

# Helpers for all entities
module ApplicationHelper
  include Pagy::Frontend
end
