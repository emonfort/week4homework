# frozen_string_literal: true

# Product model class.
# Represents the items that the store sells
class Product < ApplicationRecord
  has_one_attached :image do |attachable|
    attachable.variant resize_to_limit: [220, 220]
  end
  has_many :order_items, dependent: :nullify
  has_many :comments, as: :commentable, dependent: :destroy
  has_many :likes, dependent: :destroy
  has_and_belongs_to_many :tags, dependent: :nil

  validates :name, :sku, presence: true
  validates :sku, uniqueness: true
  validates :price, numericality: { greater_than_or_equal_to: 0.01 }
  validates :stock, numericality: { greater_than_or_equal_to: 0 }

  # Returns true if there is an admin logged in
  # Receives a +user+ as parameter
  def editable_by?(current_user)
    !!current_user && (current_user.admin? || current_user.support?)
  end
end
