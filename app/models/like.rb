# frozen_string_literal: true

# Like model class
class Like < ApplicationRecord
  belongs_to :user
  belongs_to :product

  validates :user_id, uniqueness: { scope: :product_id }
end
