# frozen_string_literal: true

# Order item model class.
# Represents the items of the purchase
class OrderItem < ApplicationRecord
  belongs_to :product, optional: true
  belongs_to :order, optional: true
  belongs_to :cart, optional: true

  delegate :name, :price, to: :product, prefix: true

  # Returns the total price of the order item
  def total_price
    product.price * quantity
  end
end
