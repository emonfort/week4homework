# frozen_string_literal: true

# Order model class
class Order < ApplicationRecord
  belongs_to :user
  has_many :order_items, dependent: :destroy
  has_many :comments, as: :commentable, dependent: :destroy

  enum status: { processing: 0, completed: 1, canceled: 2, failed: 3 }
end
