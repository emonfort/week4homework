# frozen_string_literal: true

# Cart model class.
# Represents a shopping cart, where you cand add products before buying them
class Cart < ApplicationRecord
  has_many :order_items, dependent: :destroy

  # Sums the prices of each order items and returns the total price
  def total_price
    order_items.to_a.sum(&:total_price)
  end
end
