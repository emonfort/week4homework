# frozen_string_literal: true

# Comment class
class Comment < ApplicationRecord
  belongs_to :commentable, polymorphic: true

  validates :description, presence: true
end
