# frozen_string_literal: true

# Parent class of models
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
end
