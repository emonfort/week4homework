# frozen_string_literal: true

# Query object for orders
class OrdersQuery
  attr_reader :relation

  def initialize(relation = Order.all)
    @relation = relation
  end

  # Receives an +user+
  # Returns the order from the +user+
  def user_orders(user)
    relation.where(user_id: user.id)
  end
end
