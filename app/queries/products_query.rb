# frozen_string_literal: true

# Query object for products
class ProductsQuery
  attr_reader :relation

  def initialize(relation = Product.all)
    @relation = relation
  end

  # Receives the +params+ hash
  # Returns the products order by +params+
  delegate :order, to: :relation
end
