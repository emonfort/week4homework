Rails.application.routes.draw do
  resources :users, only: [:index, :edit, :update]
  resources :order_items
  resources :orders do
    resources :comments, module: :orders
  end
  resources :carts, only: [:show, :destroy]
  resources :products do
    resources :comments, module: :products
  end
  resources :tags
  resources :likes, only: [:create, :destroy]
  root 'home#index', as: 'home_index'

  namespace :api do    
    namespace :v1 do
      resources :products, only: [:index, :show]
      resources :orders, only: [:index]
      resources :likes, only: [:create]
      post 'auth/login', to: 'authentication#login'
    end
  end

  # devise_for :users
  devise_for :users, controllers: {
    sessions: 'users/sessions'
  }
end
