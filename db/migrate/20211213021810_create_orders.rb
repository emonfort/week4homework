class CreateOrders < ActiveRecord::Migration[6.1]
  def change
    create_table :orders do |t|
      t.belongs_to :user, null: false, foreign_key: true
      t.date :date
      t.decimal :total, precision: 5, scale: 2
      t.integer :status

      t.timestamps
    end
  end
end
