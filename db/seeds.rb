10.times do
  Product.create(
    sku: Faker::Number.number(digits: 5),
    name: Faker::Commerce.product_name,
    description: Faker::Lorem.paragraph(sentence_count:3),
    price: Faker::Commerce.price(range: 10..100.0),
    stock: Faker::Number.between(from: 1, to: 25)
  )
end
